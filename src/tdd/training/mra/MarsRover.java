package tdd.training.mra;

import java.util.List;
import java.util.ArrayList;

public class MarsRover {

	
	int firstcoor=0;
	int secondcoor=0;
	List<String> problem = new ArrayList<>();
	String position="N";
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX>=0 && planetY>=0) {
			this.firstcoor=planetX;
			this.secondcoor=planetY;
			this.problem=planetObstacles;
			firstcoor = 0;
			secondcoor = 0;
		}
		else throw new MarsRoverException("Dimension not correct");
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(x>0 || y>0) {
			for(int i=0; i<problem.size(); i++) {
				
				char first = problem.get(i).charAt(1);
				int coorx = Character.getNumericValue(first);
				
				char second = problem.get(i).charAt(3);
				int coory = Character.getNumericValue(second);
				
				if(coorx == x && coory == y) {
					return true;
				}
			}
			return false;
		}
		else throw new MarsRoverException("Dimension not correct");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String lastbracket = ")";
		String firstbracket = "(";
		String comma =",";
		
		for (int i=0; i<commandString.length(); i++) {
			char value = commandString.charAt(i);
			String val = Character.toString(value);
			switch (val) {
			
			case "":
				firstcoor = 0;
				secondcoor = 0;
				position = "N";
				break;
				
			case "r":
				position = "E";
				break;
				
			case "l":
				position = "W";
				break;
				
			case "b":
				if(position.equals("N")) {
					if(secondcoor==0) {
						secondcoor = 9;
					}
					else secondcoor--;
				}
				else if(position.equals("E")) {
					if(firstcoor==0) {
						firstcoor = 9;
					}
					else firstcoor--;
				}
				else if(position.equals("W")) {
					if(firstcoor==9) {
						firstcoor = 0;
					}
					else firstcoor++;
				}
				break;
				
			case "f":
				if(position.equals("N")) {
					if(secondcoor==9) {
						secondcoor = 0;
					}
					else secondcoor++;
				}
				else if(position.equals("E")) {
					if(firstcoor==9) {
						firstcoor = 0;
					}
					else firstcoor++;
				}
				else if(position.equals("W")) {
					if(firstcoor==0) {
						firstcoor = 9;
					}
					else firstcoor--;
				}
				break;
				
			default :
				throw new MarsRoverException("Command not correct");
			}
			
		}
		
		
		String landingp = firstbracket.concat(String.valueOf(firstcoor)).concat(comma).concat(String.valueOf(secondcoor)).concat(comma).concat(position).concat(lastbracket);
		return landingp;
	}
}
