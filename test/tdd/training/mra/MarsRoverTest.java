package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetWithObstacle() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		boolean obstacle2 = rover.planetContainsObstacleAt(2, 3);
		
		assertTrue(obstacle);
		assertTrue(obstacle2);
	}
	
	@Test(expected = MarsRoverException.class)
	public void testPlanetWithErrorObstacle() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(-1, -7);
	}
	
	@Test
	public void testEmptyCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,N)",rover.executeCommand(""));
	}
	
	@Test(expected = MarsRoverException.class)
	public void testErrorCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		rover.executeCommand("zzz");
	}
	
	@Test
	public void testRCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,E)",rover.executeCommand("r"));
	}
	
	@Test
	public void testLCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
	
	@Test
	public void testFCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,1,N)",rover.executeCommand("f"));
	}
	
	@Test
	public void testLongCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(2,2,E)",rover.executeCommand("ffrrff"));
	}
	
	@Test
	public void testBCommand() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		//mi posiziono nella casella (0,2,E)
		rover.executeCommand("ff");
		
		assertEquals("(0,1,N)",rover.executeCommand("b"));
	}
	
	@Test
	public void testSphericalPlanet() throws MarsRoverException {
		
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,9,N)",rover.executeCommand("b"));
	}

}
